<html>
<head>
    <Title>GET method</Title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<form class="form-horizontal" action="captureGet.php" method="get">
    <h1 class="text-center">Registration Form</h1>
    <div class="form-group">
        <label for="fullName" class="col-sm-2 control-label">Full Name</label>
        <div class="col-sm-6">
            <input type="text" name="fname" class="form-control" id="fullName" placeholder="Full Name">
        </div>
    </div>

    <div class="form-group">
        <label for="Email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-6">
            <input type="email" name="email" class="form-control" id="Email3" placeholder="Email">
        </div>
    </div>

    <div class="form-group">
        <label for="Password3" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-6">
            <input type="password" name="password" class="form-control" id="Password3" placeholder="Password">
        </div>

    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="btn" class="btnbtn-default">Submit</button>
        </div>
    </div>
</form>
</body>

</html>
